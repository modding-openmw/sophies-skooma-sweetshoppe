#### 03 Sophie's Normal Maps for Morrowind Telvanni Patch Telvanni Housepod Heightmap Replacement

##### 03 Author 
DassiD, Sophie

##### 03 About
Fixes several issues with the Telvanni normal map textures from [Normal Maps for Morrowind](https://www.nexusmods.com/morrowind/mods/45336)

The mod cleans up some UV warping issues caused by heightmaps. It replaces the heightmaps with regular normals.

**Note** that you have to delete the following files from your Normal Maps for Morrowind folder to achieve the optimal result. Due to engine priority you cannot solve this with load order. Please delete:

```
Tx_emperor_parasol_01_nh.dds
Tx_emperor_parasol_02_nh.dds
Tx_emperor_parasol_03_nh.dds
tx_telv_roof01_nh.dds
```

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 03 Load Order

```
...
data="/home/username/games/openmw/Mods/TexturePacks/NormalMapsforMorrowind/03 Telvanni"
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/03 Sophie's Normal Maps for Morrowind Telvanni Patch Telvanni Housepod Heighmap Replacement".
```
##### 03 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/03%20Sophie's%20Normal%20Maps%20for%20Morrowind%20Telvanni%20Patch%20Telvanni%20Housepod%20Heighmap%20Replacement)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

