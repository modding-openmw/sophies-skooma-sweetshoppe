#### 02 Sophie's Hircine's Artifacts Hircine Spear Upscaled Fixed and Retextured

##### 02 Author 
 
Sophie, Leyawynn

##### 02 About

Based on Hircine's Spear from [Hircine's Artifacts](https://www.nexusmods.com/morrowind/mods/47671). Using the original mod is recomended as it includes lots of other cool things,
but is not required. Retextures, upscales, and fixes Hircine's Spear for use in the OpenMW engine primarily, but should work fine in vanilla.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 02 Load Order

```
...
data="/home/username/games/openmw/Mods/ObjectsClutter\HircinesArtifacts\Hircine's Artifacts"
...
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/02 Sophie's Hircine's Artifacts Hircine Spear Upscaled Fixed and Retextured"
...
```

##### 02 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/02%20Sophie's%20Hircine's%20Artifacts%20Hircine%20Spear%20Upscaled%20Fixed%20and%20Retextured?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

