#### 10 Shadow Mimicry's Backpack Retexture (host for modding-openmw.com)

##### 10 Author 
Shadow Mimicry, Danae

##### 10 About

A host for easy use on modding-openmw.com modlists. A backpack rextexture by Shadow Mimicry intended to be used with [Adventurer's Backpacks](https://www.nexusmods.com/morrowind/mods/43213) by Danae.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 10 Load Order

```
data="/home/username/games/openmw/Mods/Gameplay\AdventurersBackpacks".
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/10 Shadow Mimicry's Backpack Retexture (host for modding-openmw.com)".
...
```

##### 10 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/10%20Shadow%20Mimicry's%20Backpack%20Retexture%20(host%20for%20modding-openmw.com)?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

