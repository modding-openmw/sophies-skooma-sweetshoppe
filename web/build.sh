#!/usr/bin/env bash
set -eu -o pipefail

#
# This script builds the project website. It downloads soupault as needed and
# then runs it, the built website can be found at: web/build
#

this_dir=$(realpath "$(dirname "${0}")")
cd "${this_dir}"

soupault_version=4.5.0
soupault_pkg=soupault-${soupault_version}-linux-x86_64.tar.gz
soupault_path=./soupault-${soupault_version}-linux-x86_64

shas=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/sha256sums
spdl=https://github.com/PataphysicalSociety/soupault/releases/download/${soupault_version}/${soupault_pkg}

if ! [ -f ${soupault_path}/soupault ]; then
    wget ${shas}
    wget ${spdl}
    tar xf ${soupault_pkg}
    grep linux sha256sums | sha256sum -c -
fi


{
    cat ../00\ HiRez\ Creatures\ -\ Netch\ Normal\ and\ Specular\ Mapped/README.md
    cat ../01\ Sophie\'s\ Buoyant\ Armiger\ Armor\ Glow\ Patch/README.md
    cat ../02\ Sophie\'s\ Hircine\'s\ Artifacts\ Hircine\ Spear\ Upscaled\ Fixed\ and\ Retextured/README.md
    cat ../03\ Sophie\'s\ Normal\ Maps\ for\ Morrowind\ Telvanni\ Patch\ Telvanni\ Housepod\ Heightmap\ Replacement/README.md
    cat ../04\ Sophie\'s\ Normal\ Maps\ for\ Morrowind\ OAAB\ Tel\ Mora\ Normal\ Heightmap\ Replacement\ and\ Mesh\ Edit/README.md
    cat ../05\ Sophie\'s\ SM\ Bittercoast\ Tree\ Upscale/README.md
    cat ../06\ Sophie\'s\ Tx_MA_lava05A\ Normal\ Map\ "(Heightmap Replacement)"/README.md
    cat ../07\ HiRez\ Armor\ Style\ Cephalopod\ Retexture\ TD\ Cuirass\ Upscale\ and\ Color\ Match/README.md
    cat ../07\ Telvanni\ Cephalopod\ Armor\ TD\ Cuirass\ Upscale\ and\ Color\ Match/README.md
    cat ../08\ Hi-Rez\ Armors\ TR\ Oridnator\ Greaves\ Upscale\ and\ Color\ Match/README.md
    cat ../09\ Expansions\ Resource\ Conflicts\ "MET'D"/README.md
    cat ../10\ Shadow\ Mimicry\'s\ Backpack\ Retexture\ "(host for modding-openmw.com)"/README.md
    cat ../11\ SophiasClearBlueSkies/README.md
    cat ../12\ Sophie\'s\ Concept\ Art\ Ald\ Skaar\ "8k"\ Upscale/README.md
    cat ../13\ Vanilla\ Friendly\ West\ Gash\ Trees\ Normal\ Mapped/README.md
    cat ../14\ Forgotten\ Shields\ Artifacts\ Upscaled\ Textures,\ Mesh\ fixes,\ and\ Normals/README.md
    cat ../15\ Dying\ Moons\ New\ Starfields\ Upscaled/README.md
    cat ../16\ QCVL\ Blood\ Altar\ x\ Diverse\ Blood\ Fix/README.md
    cat ../17\ Missing\ Vivec\ and\ Velothi\ Arkitektora\ Vol\ 2\ Normals/README.md
    cat ../18\ 4thUnknowns\ Blighted\ Kwamas\ and\ Shalks/README.md
    cat ../19\ White\ Suran\ Normal\ and\ Specular\ Mapped/README.md
    cat ../20\ Arkitektora\ White\ Suran\ Normal\ and\ Specular\ Mapped/README.md
    cat ../21\ Akulukhan\'s\ Best\ Chamber\ OpenMW/README.md
    cat ../22\ Remiros\ Robe\ Overhaul\ Normal\ Mapped/README.md
    cat ../23\ Telvanni\ Mushroomtop\ Housepod\ UV\ Fix/README.md
    cat ../24\ Mackom\ Style\ Heads\ for\ Tamriel\ Data\ Normal\ Mapped/README.md
} | grep -Ev "^\[Nexus|^\[Patch Home" >> site/readme.md

grep -v "# Sophie's Skooma Sweetshoppe" ../CHANGELOG.md >> site/changelog.md
echo "<div class=\"center\"><img src=\"/img/openmw-cs.png\" title=\"Sophie's Skooma Sweetshoppe\" /></a></div>" > site/index.md
grep -v "# Sophie's Skooma Sweetshoppe" ../README.md >> site/index.md


PATH=${soupault_path}:$PATH soupault "$@"
