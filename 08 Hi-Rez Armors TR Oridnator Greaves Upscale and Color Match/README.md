#### 08 Hi-Rez Armors TR Oridnator Greaves Upscale and Color Match

##### 08 Author 
Settyness, Sophie, Tamriel Data Team

##### 08 About

A Color match by Settyness and an upscale of the Indoril Greaves from [Tamriel Data](https://www.tamriel-rebuilt.org/downloads/resources) by Sophie. For use with [HiRez Armors - Native Styles V2 Fixed and Optimized](https://www.nexusmods.com/morrowind/mods/47919) by Saint_Jiub and Pherim.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 08 Load Order

```
data="/home/username/games/openmw/Mods/ModdingResources\TamrielData".
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/08 Hi-Rez Armors TR Oridnator Greaves Upscale and Color Match".
...
```

##### 08 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/08%20Hi-Rez%20Armors%20TR%20Oridnator%20Greaves%20Upscale%20and%20Color%20Match)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

