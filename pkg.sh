#!/bin/sh
set -e

modname=sophies-skooma-sweetshoppe
file_name=$modname.7z

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

7z -y -mx9 a ${file_name} \
    "00 HiRez Creatures - Netch Normal and Specular Mapped" \
    "01 Sophie's Buoyant Armiger Armor Glow Patch" \
    "02 Sophie's Hircine's Artifacts Hircine Spear Upscaled Fixed and Retextured" \
    "03 Sophie's Normal Maps for Morrowind Telvanni Patch Telvanni Housepod Heightmap Replacement" \
    "04 Sophie's Normal Maps for Morrowind OAAB Tel Mora Normal Heightmap Replacement and Mesh Edit" \
    "05 Sophie's SM Bittercoast Tree Upscale" \
    "06 Sophie's Tx_MA_lava05A Normal Map (Heightmap Replacement)" \
    "07 HiRez Armor Style Cephalopod Retexture TD Cuirass Upscale and Color Match" \
    "07 Telvanni Cephalopod Armor TD Cuirass Upscale and Color Match" \
    "08 Hi-Rez Armors TR Oridnator Greaves Upscale and Color Match" \
    "09 Expansions Resource Conflicts MET'D" \
    "10 Shadow Mimicry's Backpack Retexture (host for modding-openmw.com)" \
    "11 SophiasClearBlueSkies" \
    "12 Sophie's Concept Art Ald Skaar 8k Upscale" \
    "13 Vanilla Friendly West Gash Trees Normal Mapped" \
    "14 Forgotten Shields Artifacts Upscaled Textures, Mesh fixes, and Normals" \
    "15 Dying Moons New Starfields Upscaled" \
    "16 QCVL Blood Altar x Diverse Blood Fix" \
    "17 Missing Vivec and Velothi Arkitektora Vol 2 Normals" \
    "18 4thUnknowns Blighted Kwamas and Shalks" \
    "19 White Suran Normal and Specular Mapped" \
    "20 Arkitektora White Suran Normal and Specular Mapped" \
    "21 Akulukhan's Best Chamber OpenMW" \
    "22 Remiros Robe Overhaul Normal Mapped" \
    "23 Telvanni Mushroomtop Housepod UV Fix" \
    "24 Mackom Style Heads for Tamriel Data Normal Mapped" \
    CHANGELOG.md \
    LICENSE \
    README.md \
    version.txt \
    -xr!.gitkeep

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
