#### 01 Sophie's Buoyant Armiger Armor Glow Patch

##### 01 Author
Sophie, Ivolga, Markond

##### 01 About

Adds glowmaps to the armor added by the mod [Buoyant Armigers Armor](https://www.nexusmods.com/morrowind/mods/43143).
Based on [More Glowing Mods](https://www.nexusmods.com/morrowind/mods/53504). Requires the Glass Glowset .dds texture files from [More Glow Mods](https://www.nexusmods.com/morrowind/mods/53504) or using ordenador and converting the textures from [Glass Glowset](https://www.nexusmods.com/morrowind/mods/42762A) different take that tries to blend the reflection glow map effects and includes the boot mesh.

Works for other mods that include the Buoyant Armigers Armor, like [NOD - NPC Outfit Diversity](https://www.nexusmods.com/morrowind/mods/52091).

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 01 Load Order

```
...
data="/home/username/games/openmw/Mods/Armor/BuoyantArmigersArmor"
...
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/01 Sophie's Buoyant Armiger Armor Glow Patch
...
```

##### 01 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/01%20Sophie's%20Buoyant%20Armiger%20Armor%20Glow%20Patch?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

