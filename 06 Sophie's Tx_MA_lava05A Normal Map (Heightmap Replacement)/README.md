#### 06 Sophie's Tx_MA_lava05A Normal Map (Heightmap Replacement)


##### 06 Author 
Sophie, Lougain

##### 06 About

A normal map generated from [Lougain's Landscape Reexture](https://www.nexusmods.com/morrowind/mods/42575) using Materialze. Fixes uv warping that the heightmap causes if mods use it in their mesh. The original mod is suggested to be used with this.
Due to engine priority you cannot solve this with load order. Please Delete the following heightmap from any mod that includes this such as [Lougain's Landscape Retexture](https://www.nexusmods.com/morrowind/mods/42575) or [Normal Maps for Morrowind](https://www.nexusmods.com/morrowind/mods/45336).

```
Tx_MA_lava05A_nh.dds

```

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 06 Load Order

```
Any normalmap mod that includes Tx_MA_lava05A_nh.dds
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/06 Sophie's Tx_MA_lava05A Normal Map (Heightmap Replacement)".
...
```

##### 06 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/06%20Sophie's%20Tx_MA_lava05A%20Normal%20Map%20(Heightmap%20Replacement))
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

