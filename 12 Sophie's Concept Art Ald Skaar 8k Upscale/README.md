#### 12 Sophie's Concept Art Ald Skaar 8k Upscale


##### 12 Author
Sophie, Randompal

##### 12 About

Upscales textures of the Skar building in Ald'ruhn from [Beautiful Cities of Morrowind](https://www.nexusmods.com/morrowind/mods/49231) to the 8K resolution.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 12 Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/00 Core"
...
data="/home/username/games/openmw/Mods/CitiesTowns/BeautifulCitiesofMorrowind/06 HD Textures"
...
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/12 Sophie's Concept Art Ald Skaar 8k Upscale"
...
```

##### 12 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/12%20Sophie's%20Concept%20Art%20Ald%20Skaar%208k%20Upscale?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

