#### 24 Mackom Style Heads for Tamriel Data Normal Mapped

##### 24 Author
Ronik (Hosted on Behalf), Mackom, Various Authors

##### 24 About
Mackom Style Heads for Tamriel Data Normal Mapped

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### 24 Load Order

```
...
C:\games\OpenMWMods\TexturePacks\NormalMapsforEverything\TR_PC_SHOTN Normal Mapped Ordenadored by Me
...
C:\games\OpenMWMods\TexturePacks\sophies-skoomasweetshoppe\
...
```

##### 24 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/24%20McKom%20Style%20Heads%20for%20Tamriel%20Data%20Normal%20Mapped?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
