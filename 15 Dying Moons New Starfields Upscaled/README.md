#### 15 Dying Moons New Starfields Upscaled

##### 15 Author 
Tyddy, Malchior Dahrk, Sophie

##### 15 About
Pretty skybox upscales of both of the mods.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### 15 Load Order

```
...
data="C:/SkoomaCity/omwnation/Skies/NewStarfields/00 Core
...
data="C:/SkoomaCity/omwnation/TexturePacks/sophies-skooma-sweetshoppe/15 Dying Moons New Starfields Upscaled
...
```

##### 15 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/15%20Dying%20Moons%20New%20Starfields%20Upscaled?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
