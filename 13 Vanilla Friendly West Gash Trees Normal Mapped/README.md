#### 13 Vanilla Friendly West Gash Normal Mapped

##### 13 Author 
Sophie, Melchior Dahrk

##### 13 About

Normal Maps for the updated version of [Vanilla Friendly West Gash Trees](https://www.nexusmods.com/morrowind/mods/44173?tab=files) based on the hd and leafy varient textures.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 13 Load Order

```
...
data="C:\SkoomaCity\omwnation\Trees\VanillafriendlyWestGashTreeReplacer\00 Core"
...
data="C:\SkoomaCity\omwnation\Trees\VanillafriendlyWestGashTreeReplacer\01 HD Textures"
...
data="C:\SkoomaCity\omwnation\Trees\VanillafriendlyWestGashTreeReplacer\02 Leafy texture"
...
data="C:\SkoomaCity\omwnation\TexturePacks\SophiesSkoomaSweetshoppe\13 Vanilla Friendly West Gash Trees Normal Mapped"
```

##### 13 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/13%20Vanilla%20Friendly%20West%20Gash%20Trees%20Normal%20Mapped)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
