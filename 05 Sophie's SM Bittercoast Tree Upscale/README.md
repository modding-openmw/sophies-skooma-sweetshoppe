#### 05 Sophie's SM Bittercoast Tree Upscale


##### 05 Author 
Sophie, Shadow Mimicry

##### 05 About

A simple upscale of [SM Bittercoast Tree Replacer](https://www.nexusmods.com/morrowind/mods/49883) at 2x the resolution.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 05 Load Order

```
...
data="/home/username/games/openmw/Mods/Trees/SMBitterCoastTreeReplacer"
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/05 Sophie's SM Bittercoast Tree Upscale".
...
```

##### 05 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/05%20Sophie's%20SM%20Bittercoast%20Tree%20Upscale?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

