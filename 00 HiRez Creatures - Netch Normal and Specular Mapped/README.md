#### 00 HiRez Creatures - Netch Normal and Specular Mapped

##### 00 Author
Sophie, Saint_Jiub

##### 00 About

Normal and Specular maps for [HiRez Creatures - Netch](https://www.nexusmods.com/morrowind/mods/46421) by Saint_Jiub. 


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 00 Load Order

```
...
data="/home/username/games/OpenMWMods/TexturePacks/NormalMapsforEverything/Morrowind Enhanced Textures Normal Mapped"
...
data="/home/lain/MoonsugarMountain/sophies-skooma-sweetshoppe/00 HD Netch Normal and Specular Mapped"
...
```

##### 00 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/00%20HD%20Netch%20Normal%20and%20Specular%20Mapped?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

