#### 18 4thUnknowns Blighted Kwamas and Shalk (Hosted for Ronik)

##### 18 Author
4thUnknown, Ronik

##### 18 About
Provides blighted 4thunkown varients to go along with the other blighted 4thunkown creature varients.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 18 Load Order

Requires `BlightedAnimalsRetextured.esps` from the mod [Blighted Animals Retextured]("https://modding-openmw.com/mods/blighted-animals-retextured/")

```
...
data="/home/lain/games/OpenMW/starwind-modded/Creatures/BlightedAnimalsRetextured"
...
data="/home/lain/games/OpenMW/sophies-skooma-sweetshoppe/18 4thUnknowns Blighted Kwamas and Shalk (Hosted for Ronik)"
...
```

##### 18 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/18%204thUnknowns%20Blighted%20Kwamas%20and%20Shalks?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
