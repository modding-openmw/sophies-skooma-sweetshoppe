# Sophie's Skooma Sweetshoppe

Hi I'm Sophie, and welcome to my repository. I enjoy  things like textures, meshes, upscales and normal maps and that's what you'll find here. You'll find some original mods entirely as well. You'll also find many mod patches that I've done in [MOMW Patches](https://modding-openmw.gitlab.io/momw-patches/)
. Please feel free to submit any issue you find, especially formating issues here on gitlab. Thank you! Have fun <3

#### Credits

Main Author: **Sophie**

Special Thanks: 

Johnnyhostile - For everything they've done for the community and all the work put into modding-openmw.com, one of my favorite websites that exists, and one I've been modding away at for many many years. I'm glad that I am finally in a position to give back for all the years of fun I enjoyed. The original idea behind the Skies IV/Zesterers volumetric clouds patch "Sophia's Clear Blue Skies". For helping me really love and get into modding with the best morrowind stream and vibes around. Being a part of the modding-openmw.com project has really had a positive impact on my life.

Ronik- For being on the same schedule as me and debating on countless details in relation to the modlists and the website, which is one of my favorite parts about coming home from work. The amount of time and love put into the modding-openmw.com project and Tamriel Rebuilt from them is just inspiring in general and both projects wouldn't be the same without them. Also thanks for fixing my highass mistakes now and then =).

Gonzo- For spending many hours teaching me gitlab so I can contribute to the modding-openmw.com project and for creating the awesome windows tool that I use everytime (docker sucked). For an absolute insane amount of commits leading up the 6.x release which let us have a holiday release. Gonzo walked 6.x so 6.x could run, truely.

S3ctor- For being the nicest person in the community by far. For always helping me with basically anything, whenever I need it. For Starwind, Open-CS work, and modding-openmw.com contributions. For taking any meme idea I ever have to the HIGHest and funniest level possible. The amount of projects and contributions this dood manages to comeup with create and manage is just insane. A true alien and pillar of the community. Watching their interactions with others has made me a better person.

Settyness- For helping me color match some textures in my Tamriel Data cuirass upscales many, many, times over so we ended up with super cool results. For joining the project when I asked and already contributing super cool things all the time. For being super fun to talk with in general.

Hurdrax Custos- For having the best voice probably in the Morrowind community and all the work put into the mentor month and interviews of recent. For helping the project stay grounded and being a true "brother" to everyone in the community.

Mod Repository Specific Credits:

Markond- For creating "More Glowing Mods" which inspired me to do my own take on a glowset patch for "Ivolga's Buyoant Armiger Armour" mod.

Ivolga- For creating "Ivolga's Buyoant Armiger Armour" mod which my glowset patch patches.

Soldifire and Darknut- For creating "Glass glowset" which I used some textures from for my Buoyant Armiger Armor Glowset patch.

Leyawynn- For creating "Hircine's Artifacts" which my retexture and fix for openmw is based on.

Project Enhance - For creating and maintaing "Morrowind Enhanced Textures". The best base texture mod around and one I use for textures in my "Hircine's Spear Retexture, Upscale, and Fix" mod and "Expansion Resource Conflicts Met'd".

DassiD - For creating "normal maps for morrowind" and having awesome heightmaps for Tydy's rexture mods. This was the mod that really got me staring at normals in general.

Melchior Dahrk- For creating OAAB Tel Mora. My favorite individual city mod visually of all time. The bridges patch was really fun to make and they sure look good.

Static- For helping me with an upscale request which lead me to want to learn gigapixel ai and do it myself. 

sam642- Creator of "Normal Maps for Everything in OpenMW". I use some of these in my telvanni replacement normal patches to fix warping issues caused by heightmaps.

ShadowMimcry- For being one of the most opensource moders in the community with their work and letting me upscale their awesome "SM Bittercoast Trees" mod and also for letting us host and use "SM Backpack Retexture" in our modlists.

Lougian- For creating "Landscape Rexture" who's awesome lava texture I materialized a normal map from in my "Tx_MA_lava05A Normal Map (Heightmap Replacement)" patch.

Tamriel Data/Tamriel Rebuilt team - For having the best mod and assset resource of all time and which my "TD cuirass color match and upscale" and "Hi-Rez Armors TR Oridnator Greaves Upscale and Color Match" patches are based upon.

Danke- For creating "Telvanni Cephalopod Armor" which is super cool and look way better then the Hlaalu armor on Telvanni guards.

C3pa- For creating "Cephalopod Armor Retexture - Hirez Armors Style" which is a super cool take on SiberianCrab's version of "Telvanni Cephalopod Armor".

hardek- For creating "Expansion Resource conflicts" who's meshes I patch with "Morrowind Enhanced Textures". This mod is super handy in large modlists.

Randompal- For letting me upscale the Concept Art Ald Skaar textures and always patching their mods. This is why we use Beautiful Cities of Morrowind! >:D

Zesterer- For "Volumetric Clouds" being an absolute essential piece of the openmw skybox in my opinion.

Markel9875- For creating "Skies IV", another absolutely essential piece of the openmw skybox and the mesh used in my "Sophia's Clear Blue Skies" mod.

hicks233- For creating "Alternate Skies" and having the best clear blue sky texture in the whole community which I use in "Sophia's Clear Blue Skies" mod.

























#### Web

[Project Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

<!-- [Nexus Mods](https://www.nexusmods.com/morrowind/mods/#TODO) -->

[Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe)

#### Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Gameplay\sophies-skooma-sweetshoppe

        # Linux
        /home/username/games/OpenMWMods/Gameplay/sophies-skooma-sweetshoppe

        # macOS
        /Users/username/games/OpenMWMods/Gameplay/sophies-skooma-sweetshoppe

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/issues)
* Contact the author on Discord: `@Sophie<3~`
