#### 07 Telvanni Cephalopod Armor TD Cuirass Upscale and Color Match

##### 07B Author 
Settyness, Sophie, Tamriel Data Team

##### 07B About

A color match by Settyness and an upscale of the Telvanni Cephalopod Cuirass from [Tamriel Data](https://www.tamriel-rebuilt.org/downloads/resources) by Sophie. For use with [Telvanni Cephalopod Armor](https://www.nexusmods.com/morrowind/mods/44062) by Danke and [Morrowind Enhanced Textures](https://www.nexusmods.com/morrowind/mods/46221). Use only this version of the module or the other but not both.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 07B Load Order

```
data="/home/username/games/openmw/Mods/ModdingResources/TamrielData".
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/07 Telvanni Cephalopod Armor TD Cuirass Upscale and Color Match".
...
```

##### 07B Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/07%20Telvanni%20Cephalopod%20Armor%20TD%20Cuirass%20Upscale%20and%20Color%20Match)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

