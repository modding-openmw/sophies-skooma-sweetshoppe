#### 09 Expansions Resource Conflicts MET'D



##### 09 Author
Sophie, hardek, Team Enhanced

##### 09 About

Rebundles selected textures from [Morrowind Enhanced Textures](https://www.nexusmods.com/morrowind/mods/46221) so they can be easily loaded after fixes from [Expansion Resource Conflicts](https://www.nexusmods.com/morrowind/mods/44532).

Includes the following textures:

```
tx_dwrv_wall40.dds
tx_dwrv_wall40_tr.dds
tx_poison_steam.dds
tx_rough_stone_wall.dds
tx_wood.dds
tx_wood_bm.dds
```

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 09 Load Order

```
...
data="/home/username/games/openmw/Mods/TexturePacks/MorrowindEnhancedTextures/MET 6-1 main"
...
data="/home/username/games/openmw/Mods/Fixes/ExpansionResourceConflicts"
...
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/09 Expansions Resource Conflicts MET'D"
...
```

##### 09 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/09%20Expansions%20Resource%20Conflicts%20MET'D?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
