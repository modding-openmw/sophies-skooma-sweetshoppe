#### 16 QCVL Blood Altar x Diverse Blood Mesh Fix

##### 16 Author 
SpiritTauren, Sophie

##### 16 About
Fixes the blood pool texture in the blood altar that has issues displaying with Diverse Blood and adds a normal to make it look less flat.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Semi-Compatible (Not the normal maps)
* `Rebirth`: No

##### 16 Load Order

```
...
data="/home/lain/Games/OpenMWMods/GuildsFactions/QuestsforClansandVampireLegendsQCVL/Quests for Clans and Vampire Legends/Data Files"
...
data="/home/lain/MoonsugarMountain/sophies-skooma-sweetshoppe/16 QCVL Blood Altair x Diverse Blood Fix/"
...
```

##### 16 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/16%20QCVL%20Blood%20Altar%20x%20Diverse%20Blood%20Fix)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
