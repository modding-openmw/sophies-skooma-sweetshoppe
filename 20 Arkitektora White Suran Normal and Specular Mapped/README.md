#### 20 Arkitektora White Suran Normal and Specular Mapped

##### 20 Author
Malchior Dahrk, Basswalker, Tyddy, Sophie

##### 20 About
Provides normal and specular maps for [Arkitektora White Suran](https://www.nexusmods.com/morrowind/mods/45101). Based off the HD textures.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 20 Load Order


```
...
data="/home/username/games/OpenMWMods/Architecture/WhiteSuran2MDEdition/00 Core"
...
data="/home/username/games/OpenMWMods/Architecture/WhiteSuran2MDEdition/06 HD Textures"
...
data="/home/username/games/OpenMWMods/TexturePacks/SophiesSkoomaSweetshoppe/19 White Suran Normal and Specular Mapped"
...
data="/home/username/games/OpenMWMods/TexturePacks/SophiesSkoomaSweetshoppe/20 Arkitektora White Suran Normal and Specular Mapped"
```

##### 20 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/20%20Arkitektora%20White%20Suran%20Normal%20and%20Specular%20Mapped?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
