#### 07A HiRez Armor Style Cephalopod Retexture TD Cuirass Upscale and Color Match


##### 07A Author 
Settyness, Sophie, Tamriel Data Team

##### 07A About

A color match by Settyness and an upscale of the Telvanni Cephalopod Cuirass from [Tamriel Data](https://www.tamriel-rebuilt.org/downloads/resources) by Sophie. For use with [Cephalopod Armor Retexture - Hirez Armors Style]
(https://www.nexusmods.com/morrowind/mods/48658) by C3pa. Use only this version of the module or the other but not both.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: No

##### 07A Load Order

```
data="/home/username/games/openmw/Mods/ModdingResources/TamrielData".
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/07 HiRez Armor Style Cephalopod Retexture TD Cuirass Upscale and Color Match".
...
```

##### 07A Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/07%20HiRez%20Armor%20Style%20Cephalopod%20Retexture%20TD%20Cuirass%20Upscale%20and%20Color%20Match)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

