#### 14 Forgotten Shields Artifacts (Upscaled Textures, Mesh fixes, and Normals)

##### 14 Author 
Painkiller97, Razkhul, Maiq+Trunksbomb, Sophie

##### 14 About
Converts meshes to openMW to proprely take advantage of normal mapping along with upscaled textures.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 14 Load Order

```
...
data="C:\SkoomaCity\omwnation\Trees\ForgottenShieldsArtifacts
...
data="C:\SkoomaCity\omwnation\Trees\Forgotten Shields Artifacts (Upscaled Textures, Mesh fixes, and Normals)"
...
```

##### 14 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/14%20Forgotten%20Shields%20Artifacts%20Upscaled%20Textures,%20Mesh%20fixes,%20and%20Normals?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
