#### 19 White Suran Normal and Specular Mapped

##### 19 Author
Malchior Dahrk, Basswalker, Sophie

##### 19 About
Provides normal and specular maps for [White Suran 2 - MD Edition](https://www.nexusmods.com/morrowind/mods/44153/). Based off the HD textures.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 19 Load Order


```
...
data="/home/username/games/OpenMWMods/Architecture/WhiteSuran2MDEdition/00 Core"
...
data="/home/username/games/OpenMWMods/Architecture/WhiteSuran2MDEdition/06 HD Textures"
...
data="/home/username/games/OpenMWMods/TexturePacks/SophiesSkoomaSweetshoppe/19 White Suran Normal and Specular Mapped"
...
```

##### 19 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/19%20White%20Suran%20Normal%20and%20Specular%20Mapped?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
