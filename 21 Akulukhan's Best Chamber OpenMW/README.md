#### 21 Akulukhan's Best Chamber OpenMW

##### 21 Author
Vegetto, Randompal, Sophie

##### 21 About
Converts meshes and uses proper openmw normals and materials which results in an extremely less shiny experience. For use with the main version of the mod.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 21 Load Order


```
...
data="/home/username/games/OpenMWMods/Interiors/AkulukhansBestChamber/00 Core"
...
data="/home/username/games/OpenMWMods/TexturePacks/sophies-skooma-sweetshopee/21 Akulukhan's Best Chamber OpenMW"
...
```

##### 21 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/21%20Akulukhan's%20Best%20Chamber%20OpenMW?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
