#### 23 Telvanni Mushroomtop Housepod UV Fix

##### 23 Author
Sophie 

##### 23 About
Fixes UV issues on the housepod mushroomtops.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: Yes
* `Rebirth`: Yes

##### 23 Load Order

Should be good unless the mod adds bumpmaps or similar and includes the housepod meshes. Works best with Tydys Arkitektora but should be an improvement with other textures replacers. These patches will be created on request.

##### 23 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/23%20Telvanni%20Mushroomtop%20Housepod%20UV%20Fix?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
