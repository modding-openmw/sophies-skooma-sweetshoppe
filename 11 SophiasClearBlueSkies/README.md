#### 11 SophiasClearBlueSkies

##### 11 Author
Sophie, hicks233, Zesterer, Markel9875


##### 11 About
A texture replacer for [skies .iv](https://www.nexusmods.com/morrowind/mods/43311) that makes the clear skies actually clear and blue, intended for use with [Zesterer's Volumetric Cloud & Mist Mod for OpenMW](https://github.com/zesterer/openmw-volumetric-clouds).

Makes using [Zesterer's Volumetric Cloud & Mist Mod for OpenMW](https://github.com/zesterer/openmw-volumetric-clouds) with [skies .iv](https://www.nexusmods.com/morrowind/mods/43311) a bit of a more enjoyable experience.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: Yes

##### 11 Load Order

```
...
data="/home/username/games/openmw/Mods/Skies/SkiesIV/Skies - .IV"
...
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/11 SophiasClearBlueSkies"
...
```

##### 11 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/11%20SophiasClearBlueSkies?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)

