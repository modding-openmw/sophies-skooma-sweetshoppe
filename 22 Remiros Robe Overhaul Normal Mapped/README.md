#### 22 Remiros Robe Overhaul Normal Mapped

##### 22 Author
Remiros, Sophie 

##### 22 About
Converts meshes and uses proper openmw normals and materials which results in an extremely less shiny experience. For use with the main version of the mod.

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 22 Load Order

Load after any mod that might contain the base normal textures. Otherwise load order shouldn't matter.

##### 22 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/22%20Remiros%20Robe%20Overhaul%20Normal%20Mapped?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
