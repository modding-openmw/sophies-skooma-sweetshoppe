#### 17 Missing Vivec and Velothi Aritektora Vol 2 Normals (hosted for Ronik)
##### 17 Author 
Daggerfall Team, Tyddy, Ronik

##### 17 About
Provides a few missing normal map textures missing from the normal map mod by Daggerfall Team for use with Vivec and Velothi - Arkitektora Vol.2.


**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 17 Load Order

```
...
Any normal map mod that contains the textures in this mod.
...
data="/home/lain/games/OpenMW/sophies-skooma-sweetshoppe/17 Missing Vivec and Velothi Aritektora Vol 2 Normals"
...
```

##### 17 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/17%20Missing%20Vivec%20and%20Velothi%20Arkitektora%20Vol%202%20Normals?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe/)
