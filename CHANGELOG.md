## Sophie's Skooma Sweetshoppe Changelog

#### 1.0

* Initial version of the mod.

#### 1.1

* Added Vanilla Friendly West Gash Trees Normal Mapped

#### 1.24

* Added 14 Forgotten Shields Artifacts Upscaled Textures, Mesh Fixes, and Normals.

* Added 15 Dying Moons New Starfields Upscaled

* Added 16 QCVL Blood altair x Diverse Blood

* Added 17 Missing Vivec and Velothi Aritektora Vol 2 Normals

#### 1.3

* Added 18 4thUnknowns Blighted Kwamas and Shulk

#### 1.4

* Added 19 White Suran Normal And Specular Mapped

* Added 20 Arkitektora White Suran Normal and Spec Mapped

#### 1.5

* Added 21 Akulukhan's Best Chamber 00core for OpenMW

* Added 22 Remiros Robe Overhaul Normal Mapped

* Added 23 Telvanni Mushroomtop Housepod UV Fix

#### 1.51

* Added an additonal heightmap patch to telvanni module.

#### 1.61

* Added HiRez Creatures - Netch Normal and Specular Mapped option, fix BAIN

#### 1.70

* Added Mackom Style Heads for Tamriel Data Normal Mapped

* Improved Normals for SM Bittercoast Trees Upscaled