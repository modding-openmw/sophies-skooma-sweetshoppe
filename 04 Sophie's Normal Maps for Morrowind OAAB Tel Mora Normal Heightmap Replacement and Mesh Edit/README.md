#### 04 Sophie's Normal Maps for Morrowind OAAB Tel Mora Normal Heightmap Replacement and Mesh Edit

##### 04 Author 
Sophie, Melchior Dahrk, sam642

##### 04 About

Fixes several issues in the OAAB Tel Mora bridges with the heightmap map textures for tx_wood found in [Normal Maps for Morrowind](https://www.nexusmods.com/morrowind/mods/45336). Normals are provided courtesy of [Normal Maps for Everything in OpenMW](https://www.nexusmods.com/morrowind/mods/45341?tab=files)
No edit is needed since the heightmap is working keeping and mesh and unique texture paths are provided. :)

**Works with**:

* `Morrowind.exe/MGE-XE/MWSE`: No
* `Rebirth`: No

##### 04 Load Order

```
...
data="/home/username/games/openmw/Mods/CitiesTowns/OAABTelMora"
... 
data="/home/username/games/openmw/Mods/TexturePacks/sophies-skooma-sweetshoppe/04 Sophie's Normal Maps for Morrowind OAAB Tel Mora Normal Heightmap Replacement and Mesh Edit".
```
##### 04 Web

* [Source on GitLab](https://gitlab.com/modding-openmw/sophies-skooma-sweetshoppe/-/tree/master/04%20Sophie's%20Normal%20Maps%20for%20Morrowind%20OAAB%20Tel%20Mora%20Normal%20Heightmap%20Replacement%20and%20Mesh%20Edit?ref_type=heads)
* [Patch Home](https://modding-openmw.gitlab.io/sophies-skooma-sweetshoppe)

